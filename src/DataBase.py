from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import sessionmaker

from config import db_name
from time import time


engine = create_engine(db_name, echo=False, pool_recycle=7200)
Base = declarative_base()


class Message(Base):
    __tablename__ = 'messages'
    id = Column(Integer, autoincrement=True)
    message_id = Column(Integer, nullable=False)
    message_type = Column(String(64), nullable=True)
    chat_id = Column(Integer, nullable=False)
    date_of_shipment = Column(Integer)

    def __init__(self, message_id, chat_id, message_type='Text'):
        self.message_id = message_id
        self.chat_id = chat_id
        self.message_type = message_type
        self.date_of_shipment = int(time())

    def __repr__(self):
        return f"Message {self.id} <{self.message_id}, {self.date_of_shipment}>"


Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)

